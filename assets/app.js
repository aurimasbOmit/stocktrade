/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

// any CSS you import will output into a single css file (app.css in this case)
import './styles/app.css';

// start the Stimulus application
import './bootstrap';

// alert('15');

document.getElementById('stockName').addEventListener("click", function () {
    alert('15');
});

//veikia ajax start
// $.ajax({
//     url: '/stocks/stock-import',
//     type: 'POST',
//     dataType: 'json',
//     data: {},
//     success: (response) => {
//         // alert('succeeded');
//     },
//     error: (response) => {
//         // alert('failed');
//     }
// });

// window.setTimeout(function () {
//   window.location.reload();
// }, 3000);
//veikia ajax end

//veikia intervalinis ajax start
// var stockImport = setInterval(function () {
//   $.ajax({
//     url: '/stocks/stock-import',
//     type: 'POST',
//     dataType: 'json',
//     data: {},
//     success: (response) => {
//       // alert('succeeded');
//     },
//     error: (response) => {
//       // alert('failed');
//     }
//   });
//   window.setTimeout(function () {
//     window.location.reload();
//   }, 3000);
// }, 1200000)
//veikia intervalinis ajax end


