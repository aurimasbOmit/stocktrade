<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210102160639 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE IF NOT EXISTS stock_transactions (id INT AUTO_INCREMENT NOT NULL, stock_id_id INT NOT NULL, bid DOUBLE PRECISION NOT NULL, ask DOUBLE PRECISION NOT NULL, bid_size INT NOT NULL, ask_size INT NOT NULL, currency VARCHAR(255) NOT NULL, regular_market_price DOUBLE PRECISION DEFAULT NULL, regular_market_day_low DOUBLE PRECISION DEFAULT NULL, regular_market_day_range VARCHAR(255) DEFAULT NULL, regular_market_day_high DOUBLE PRECISION DEFAULT NULL, created_at DATETIME NOT NULL, INDEX IDX_CBC3205FE35482A6 (stock_id_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE stock_transactions ADD CONSTRAINT FK_CBC3205FE35482A6 FOREIGN KEY (stock_id_id) REFERENCES stock (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE stock_transactions');
    }
}
