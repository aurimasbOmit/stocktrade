<?php

namespace App\Entity;

use App\Entity\Stock;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=StockTransactionsRepository::class)
 */
class StockTransactions
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Stock::class, inversedBy="stockTransactions")
     * @ORM\JoinColumn(nullable=false)
     */
    private $stockId;

    /**
     * @ORM\Column(type="float")
     */
    private $bid;

    /**
     * @ORM\Column(type="float")
     */
    private $ask;

    /**
     * @ORM\Column(type="integer")
     */
    private $bidSize;

    /**
     * @ORM\Column(type="integer")
     */
    private $askSize;

    /**
     * @ORM\Column(type="string")
     */
    private $currency;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $regularMarketPrice;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $regularMarketDayLow;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $regularMarketDayRange;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $regularMarketDayHigh;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return \App\Entity\Stock|null
     */
    public function getStockId(): ?Stock
    {
        return $this->stockId;
    }

    /**
     * @param \App\Entity\Stock|null $stockId
     * @return $this
     */
    public function setStockId(?Stock $stockId): self
    {
        $this->stockId = $stockId;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getBid()
    {
        return $this->bid;
    }

    /**
     * @param mixed $bid
     * @return StockTransactions
     */
    public function setBid($bid): StockTransactions
    {
        $this->bid = $bid;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getAsk()
    {
        return $this->ask;
    }

    /**
     * @param mixed $ask
     * @return StockTransactions
     */
    public function setAsk($ask): StockTransactions
    {
        $this->ask = $ask;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getBidSize()
    {
        return $this->bidSize;
    }

    /**
     * @param mixed $bidSize
     * @return StockTransactions
     */
    public function setBidSize($bidSize): StockTransactions
    {
        $this->bidSize = $bidSize;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getAskSize()
    {
        return $this->askSize;
    }

    /**
     * @param mixed $askSize
     * @return StockTransactions
     */
    public function setAskSize($askSize): StockTransactions
    {
        $this->askSize = $askSize;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * @param mixed $currency
     * @return StockTransactions
     */
    public function setCurrency($currency): StockTransactions
    {
        $this->currency = $currency;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getRegularMarketPrice()
    {
        return $this->regularMarketPrice;
    }

    /**
     * @param mixed $regularMarketPrice
     * @return StockTransactions
     */
    public function setRegularMarketPrice($regularMarketPrice): StockTransactions
    {
        $this->regularMarketPrice = $regularMarketPrice;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getRegularMarketDayLow()
    {
        return $this->regularMarketDayLow;
    }

    /**
     * @param mixed $regularMarketDayLow
     * @return StockTransactions
     */
    public function setRegularMarketDayLow($regularMarketDayLow)
    {
        $this->regularMarketDayLow = $regularMarketDayLow;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getRegularMarketDayRange()
    {
        return $this->regularMarketDayRange;
    }

    /**
     * @param mixed $regularMarketDayRange
     * @return StockTransactions
     */
    public function setRegularMarketDayRange($regularMarketDayRange): StockTransactions
    {
        $this->regularMarketDayRange = $regularMarketDayRange;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getRegularMarketDayHigh()
    {
        return $this->regularMarketDayHigh;
    }

    /**
     * @param mixed $regularMarketDayHigh
     * @return StockTransactions
     */
    public function setRegularMarketDayHigh($regularMarketDayHigh): StockTransactions
    {
        $this->regularMarketDayHigh = $regularMarketDayHigh;
        return $this;
    }

    /**
     * @return \DateTimeInterface|null
     */
    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTimeInterface $createdAt
     * @return $this
     */
    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }
}
