<?php

namespace App\Repository;

use App\Entity\Stock;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\DBAL\DBALException;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\ResultSetMappingBuilder;
use Doctrine\Persistence\ManagerRegistry;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Entity;

/**
 * @method Stock|null find($id, $lockMode = null, $lockVersion = null)
 * @method Stock|null findOneBy(array $criteria, array $orderBy = null)
 * @method Stock[]    findAll()
 * @method Stock[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class StockRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Stock::class);
    }

    // /**
    //  * @return Stock[] Returns an array of Stock objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Stock
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */

    /**
     * @return false|mixed
     * @throws DBALException
     */
    public function getStockNamesByString()
    {
        $connection = $this->getEntityManager()->getConnection();

        $stocksTags = $connection->query("select group_concat(concat(tag) separator ',') from stock where disabled = 0");

        return $stocksTags->fetchColumn();


//        $queryBuilder = $this->createQueryBuilder('s')
//            ->select("select group_concat(concat(tag) separator ',') from stock");
//
//        return $queryBuilder->getQuery()->getResult();
//        $sql = "select group_concat(concat(tag) separator ',') from stock";

//        $entityManager = $this->getEntityManager();

//        $mappingBuilder = new ResultSetMappingBuilder($entityManager);
//        $mappingBuilder->addRootEntityFromClassMetadata(Stock::class, 'l');

//        return $entityManager->createNativeQuery($sql, $mappingBuilder);
    }

    public function getStockTransactionsByCreatedAtDate(\DateTimeImmutable $startDate, \DateTimeImmutable $endDate)
    {
        return $this->createQueryBuilder('stock')
            ->select('stock')
            ->innerJoin('stock.stockTransactions', 'stockTransactions')
            ->where('stockTransactions.createdAt >= :from')
            ->andWhere('stockTransactions.createdAt <= :to')
            ->setParameters([
                'from' => $startDate,
                'to' => $endDate
            ])
            ->orderBy('stockTransactions.createdAt', 'DESC')
            ->getQuery()
            ->getResult();
    }
}
