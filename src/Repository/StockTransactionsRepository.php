<?php

namespace App\Repository;

use App\Entity\StockTransactions;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method StockTransactions|null find($id, $lockMode = null, $lockVersion = null)
 * @method StockTransactions|null findOneBy(array $criteria, array $orderBy = null)
 * @method StockTransactions[]    findAll()
 * @method StockTransactions[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class StockTransactionsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, StockTransactions::class);
    }

    // /**
    //  * @return StockTransactions[] Returns an array of StockTransactions objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?StockTransactions
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */

//    public function findStockTransactionsByCreatedAtDate(\DateTimeImmutable $startDate, \DateTimeImmutable $endDate){
//        return $this->createQueryBuilder('s')
//            ->where('s.')
//    }
}
