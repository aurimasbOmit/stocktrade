<?php


namespace App\Service;


use Symfony\Component\HttpFoundation\JsonResponse;

class SenderEmail
{
    private $from = 'stock@omitas.lt';
    private $to = 'aurimas@omitas.com';
    private $mailer;

    public function __construct(\Swift_Mailer $mailer)
    {
        $this->mailer = $mailer;
    }

    /**
     * @param array $mailInformation
     * @return JsonResponse
     */
    public function sendEmail(array $mailInformation): JsonResponse
    {
        $email = new \Swift_Message();
        $email->setFrom($this->from)
            ->setTo($this->to)
            ->setSubject($mailInformation['subject']);

        if (array_key_exists('emailBody', $mailInformation)) {
            $email->setBody($mailInformation['emailBody']);
        }

        if (array_key_exists('emailPart', $mailInformation)) {
            $email->addPart($mailInformation['emailPart'], 'text/html');
        }

        if (isset($mailInformation['cc']) && is_array($mailInformation)) {
            $email->setCc($mailInformation['cc']);
        }

        try {
            $this->mailer->send($email);
        } catch (\Swift_TransportException $e) {
            return new JsonResponse(['status' => 'error', 'message' => $e->getMessage()]);
        }
        return new JsonResponse(['status' => 'success']);
    }
}