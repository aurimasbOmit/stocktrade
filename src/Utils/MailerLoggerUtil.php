<?php


namespace App\Utils;


use Psr\Log\LoggerInterface;
use Psr\Log\LogLevel;
use Swift_Events_SendEvent;

class MailerLoggerUtil implements \Swift_Events_SendListener
{
//url: '%env(MAILER_URL)%'
//spool: { type: 'memory' }
//transport: 'sendmail'
//  command: '/usr/sbin/sendmail -oi -t'
    protected $logger;

    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    public function beforeSendPerformed(Swift_Events_SendEvent $evt)
    {
        // TODO: Implement beforeSendPerformed() method.
    }

    public function sendPerformed(Swift_Events_SendEvent $evt)
    {
        $level = $this->getLoggerLevel($evt);
        $message = $evt->getMessage();

        if ($level === 'Success') {
            $level = '200';
        }

        $this->logger->log(
            $level,
            $message->getSubject() . ' - ' . $message->getId(),
            [
                'result' => $evt->getResult(),
                'subject' => $message->getSubject(),
                'to' => $message->getTo(),
                'cc' => $message->getCc()
            ]
        );
    }

    /**
     * @param Swift_Events_SendEvent $evt
     * @return string
     */
    private function getLoggerLevel(Swift_Events_SendEvent $evt): string
    {
        switch ($evt->getResult()) {
            case Swift_Events_SendEvent::RESULT_SPOOLED:
            case Swift_Events_SendEvent::RESULT_PENDING:
                return LogLevel::DEBUG;
            case Swift_Events_SendEvent::RESULT_FAILED:
                return LogLevel::CRITICAL;
            case Swift_Events_SendEvent::RESULT_TENTATIVE:
                return LogLevel::ERROR;
        }

        return 'Success';
    }
}