<?php


namespace App\Utils;


class Dates
{
    public const DATE = 'Y-m-d';

    /**
     * @param int $weekDay
     * @return bool
     */
    public static function isWeekend(int $weekDay)
    {
        return $weekDay === 6 || $weekDay === 0;
    }

    /**
     * @param $endTime
     * @param $startTime
     * @return string
     */
    public static function getExecutionTime($endTime, $startTime)
    {
        $duration = $endTime - $startTime;
        $hours = (int)($duration / 60 / 60);
        $minutes = (int)($duration / 60) - $hours * 60;
        $seconds = (int)$duration - $hours * 60 * 60 - $minutes * 60;
        return ($hours === 0 ? "00" : $hours)
            . ':' . ($minutes === 0 ? "00" : ($minutes < 10 ? "0" . $minutes : $minutes))
            . ':' . ($seconds === 0 ? "00" : ($seconds < 10 ? "0" . $seconds : $seconds));
    }
}