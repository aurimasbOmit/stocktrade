<?php


namespace App\Controller;


use App\Entity\Stock;
use App\Entity\StockTransactions;
use App\Service\SenderEmail;
use App\Utils\Dates;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Validator\Constraints\Date;

class DefaultController extends AbstractController
{

    private $region = 'US';

    public function index(SenderEmail $senderEmail)
    {
//        var_dump($senderEmail->sendEmail([
//            'stockName' => 'Tesla',
//            'cc' => ['nerijus@omitas.com', 'blazysaurimas5@gmail.com']
//        ]));
//        exit;
//        $stock = (new Stock())
//            ->setName('Apple')
//            ->setTag('APL')
//            ->setCreatedAt(new \DateTimeImmutable());
//
//        $this->getDoctrine()->getManager()->persist($stock);
//
//        $this->getDoctrine()->getManager()->flush();

        var_dump(phpversion());

        $stocks = $this->getDoctrine()->getRepository(Stock::class)->findAll();

        return $this->render('base.html.twig', [
            'stocks' => $stocks,
        ]);
    }

    /**
     * @param $stockId
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function stockOveriew($stockId)
    {
        /** @var Stock $stock */
        $stock = $this->getDoctrine()->getRepository(Stock::class)->find($stockId);

        $stockTransactions = $stock->getStockTransactions();

//        exit;

        return $this->render('stocks.html.twig', [
            'stockTransactions' => $stockTransactions,
            'stockName' => $stock->getName(),
        ]);
    }

    public function stockImport()
    {
        if (Dates::isWeekend(date('w'))) {
            return new JsonResponse(['status' => 'error', 'message' => 'Weekend']);
        }
//        $stocks = $this->getDoctrine()->getRepository(Stock::class)->findBy(['disabled' => false]);
        $stocksTagNames = $this->getDoctrine()->getRepository(Stock::class)->getStockNamesByString();

        $curl = curl_init();
//        curl_setopt_array($curl, [
//            CURLOPT_URL => "https://alpha-vantage.p.rapidapi.com/query?function=GLOBAL_QUOTE&symbol=$stocksTagNames&datatype=json",
//            CURLOPT_RETURNTRANSFER => true,
//            CURLOPT_FOLLOWLOCATION => true,
//            CURLOPT_ENCODING => "",
//            CURLOPT_MAXREDIRS => 10,
//            CURLOPT_TIMEOUT => 30,
//            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
//            CURLOPT_CUSTOMREQUEST => "GET",
//            CURLOPT_HTTPHEADER => [
//                "x-rapidapi-host: alpha-vantage.p.rapidapi.com",
//                "x-rapidapi-key: 8869cc29a2mshbf800039ece66eap14ea82jsn6e23c2f8bd14"
//            ],
//        ]);

        curl_setopt_array($curl, [
            CURLOPT_URL => "https://apidojo-yahoo-finance-v1.p.rapidapi.com/market/v2/get-quotes?region=$this->region&symbols=" . $stocksTagNames,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => [
                "x-rapidapi-host: apidojo-yahoo-finance-v1.p.rapidapi.com",
                "x-rapidapi-key: 8869cc29a2mshbf800039ece66eap14ea82jsn6e23c2f8bd14"
            ],
        ]);


        $response = json_decode(curl_exec($curl), true);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            return new JsonResponse(['status' => 'error', 'message' => 'curl Error']);
        }
//        var_dump($response);
        $stockResponseData = $response['quoteResponse']['result'];
//            var_dump('78');
//var_dump($stockResponseData);

        foreach ($stockResponseData as $responseDatum) {

            /** @var Stock $stock */
            $stock = $this->getDoctrine()->getRepository(Stock::class)->findOneBy(['tag' => $responseDatum['symbol']]);

//            var_dump($responseDatum['symbol']);
//            var_dump($responseDatum['bid']);
//            var_dump($responseDatum['bidSize']);
//            var_dump($responseDatum['ask']);
//            var_dump($responseDatum['askSize']);
//            var_dump($responseDatum['currency']);
//            var_dump($responseDatum['regularMarketPrice']);
//            var_dump($responseDatum['regularMarketDayHigh']);
//            var_dump($responseDatum['regularMarketDayLow']);
//            var_dump($responseDatum['regularMarketDayRange']);
//            var_dump($stock->getId());
//            exit;

            try {
                $stockTransaction = (new StockTransactions())
                    ->setStockId($stock)
                    ->setBid(floatval($responseDatum['bid']))
                    ->setAsk(floatval($responseDatum['ask']))
                    ->setBidSize(intval($responseDatum['bidSize']))
                    ->setAskSize(intval($responseDatum['askSize']))
                    ->setCurrency($responseDatum['currency'])
                    ->setRegularMarketPrice(floatval($responseDatum['regularMarketPrice']))
                    ->setRegularMarketDayHigh(floatval($responseDatum['regularMarketDayHigh']))
                    ->setRegularMarketDayLow(floatval($responseDatum['regularMarketDayLow']))
                    ->setRegularMarketDayRange($responseDatum['regularMarketDayRange'])
                    ->setCreatedAt(new \DateTimeImmutable());

                $this->getDoctrine()->getManager()->persist($stockTransaction);
            } catch (\Exception $e) {
                return new JsonResponse(['status' => 'error', 'message' => $e->getMessage()]);
            }
        }

        $this->getDoctrine()->getManager()->flush();

        return new JsonResponse(['status' => 'success']);
    }


}